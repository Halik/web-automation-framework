import { ElementInterface, ElementFunction } from '../../../core/interfaces';
import { Group } from '../../common/enums';

export const ChannelPageElements: ElementInterface[] = [
    {
        name: 'btnUplpad',
        isVisible: true,
        text: 'Upload',
        css: '#header > div > div._1M0Z7PVIPN5_uPtitkSCW7 > a._2bR0VNOr0505p4eSudMi0k'
    },
    {
        name: 'txtInputSearch',
        isVisible: true,
        text: '',
        css: '#searchbar > div > div > form > input'
    },
    {
        name: 'btnSearch',
        isVisible: true,
        text: '',
        css: '#searchbar > div > a'
    },
    {
        name: 'titleUpload',
        group: Group.UPLOAD,
        text: 'UPLOAD',
        css: '#react-target > div:nth-child(4) > div:nth-child(2) > div:nth-child(1) > div > div > div > div > div > h3'
    },
    {
        name: 'descUpload',
        group: Group.UPLOAD,
        text: 'Upload your GIFs and Stickers to share on Facebook, Twitter,\nInstagram, text message, email, and everywhere else.',
        css: 'div._2scyPzzrTEy7Q6WRxDKvLO'
    },
    {
        name: 'fileUpload',
        group: Group.UPLOAD,
        text: 'Browse Your Files\nUpload one or more GIFs.',
        css: 'div.QRF3vZyoeZvDoXh9oMH_s > div:nth-child(1)'
    },
    {
        name: 'dragAndDrop',
        group: Group.UPLOAD,
        text: 'Drag and Drop GIFs\nDrop them right here, right now.',
        css: 'div.QRF3vZyoeZvDoXh9oMH_s > div:nth-child(2)'
    },
    {
        name: 'lblUrl',
        group: Group.UPLOAD,
        text: 'Any URL',
        css: 'div > div.QKXDV3ZxLSzWMCh947hPw._3MtX0nrKb7z9ufN7U0tY0M'
    },
    {
        name: 'descUrl',
        group: Group.UPLOAD,
        text: 'We support YouTube, Vimeo, and many others!',
        css: 'div.ABwMSLbVpRr6-Cqcc5VIP'
    },
    {
        name: 'lnkGuid',
        group: Group.UPLOAD,
        text: 'Community Guidelines',
        url: 'https://giphy.com/community-guidelines',
        css: 'div._1_wYjMTOIeHMRSnuxAcUVs > div > a:nth-child(1)'
    },
    {
        name: 'lnkPolicy',
        group: Group.UPLOAD,
        text: 'Privacy Policy',
        url: 'https://giphy.com/privacy',
        css: 'div._1_wYjMTOIeHMRSnuxAcUVs > div > a:nth-child(2)'
    },
    {
        name: 'txtInputUrl',
        group: Group.UPLOAD,
        text: '',
        css: 'div._3R6K44M9Q_zXyORjJAgM-S > div > input'
    },
    {
        name: 'btnUploadToGiphy',
        css: 'button.sc-jffIyK.htiKJt'
    },
    {
        name: 'lblChannelCount',
        css: 'div.carousel__SectionHeaderWithPrivacy-sc-1vmii17-0.ebJEJv > h2'
    },
    {
        name: 'lnkUpload',
        css: 'div._15rz9-CUN1ck-Y3u38oYNU > a'
    },
];

export interface ChannelElementsInterface {
    btnUplpad: ElementFunction;
    txtInputSearch: ElementFunction;
    btnSearch: ElementFunction;
    fileUpload: ElementFunction;
    txtInputUrl: ElementFunction;
    btnUploadToGiphy: ElementFunction;
    lblChannelCount: ElementFunction;
    lnkUpload: ElementFunction;
}
