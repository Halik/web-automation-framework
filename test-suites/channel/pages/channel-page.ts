import { ChannelPageElements, ChannelElementsInterface } from '.';
import { BasePage } from '../../../core/pages';
import { Group } from '../../common/enums';

export class ChannelPage extends BasePage {
    public elements: ChannelElementsInterface;
    public uploadElements = this.getElementsArrayByGroup(Group.UPLOAD);

    constructor() {
        super(ChannelPageElements);
    }

    public async waitForBtnUploadVisibilty(isVisable = true): Promise<void> {
        isVisable ? await this.elements.btnUplpad().waitForVisibility()
            : await this.elements.btnUplpad().waitForInvisibility();
    }

    public async waitForBtnUploadToGiphyVisibilty(isVisable = true): Promise<void> {
        isVisable ? await this.elements.btnUploadToGiphy().waitForVisibility()
            : await this.elements.btnUploadToGiphy().waitForInvisibility();
    }

    public async waitForLblChannelCountVisibilty(isVisable = true): Promise<void> {
        isVisable ? await this.elements.lblChannelCount().waitForVisibility()
            : await this.elements.lblChannelCount().waitForInvisibility();
    }

    public async getTextLblChannel(): Promise<string> {
        return await this.elements.lblChannelCount().getText();
    }

    public async isLblChannelCountVisible(): Promise<boolean> {
        return await this.elements.lblChannelCount().isVisible();
    }

    public async clickOnBtnUpload(): Promise<void> {
        await this.elements.btnUplpad().click();
    }

    public async clickOnBtnUploadToGiphy(): Promise<void> {
        await this.elements.btnUploadToGiphy().click();
    }

    public async clickOnBtnSearch(): Promise<void> {
        await this.elements.btnSearch().click();
    }

    public async uploadByUrl(url: string): Promise<void> {
        await this.elements.txtInputUrl().waitForVisibility();
        await this.elements.txtInputUrl().sendKeys(url);
    }

    public async getLnkUpload(): Promise<string> {
        return await this.elements.lnkUpload().getAttribute('href');
    }

    public async fillTxtSearch(text: string): Promise<void> {
        await this.elements.txtInputSearch().setText(text);
    }
}
