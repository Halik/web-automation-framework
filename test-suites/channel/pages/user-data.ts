import { UserInterface } from '../../../core/interfaces';

export const Users: { [key: string]: UserInterface } = {
    Alex: {
        username: 'alik.hakobyan@gmail.com',
        password: '123456',
        testData: {
            validSearch: 'sport',
            invalidSearch: 'txtsport'
        }
    }
};
