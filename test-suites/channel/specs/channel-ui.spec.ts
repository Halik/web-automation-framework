import { baseUrl } from '../../common/constants';
import { Login } from '../../../core/specs/login.page.spec';
import { Users } from '../pages';
import { ChannelPage } from '../pages';
import { URL } from '../../common/enums';

const page = new ChannelPage();
const user = Users.Alex;
Login.sharedSetup(user, baseUrl);

describe('Channel: UI Tests: ', () => {

    beforeAll(async () => {
        try {
            await page.waitForBtnUploadVisibilty();
        }
        catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('1. Should check initial ui elements visibility and texts', async function () {
        try {
            expect(await page.checkDefaultUIElementsVisibility()).toBe(true, 'upload/search elements are visibile');
            expect(await page.checkDefaultTextsOnPage()).toBe(true, 'upload/search element\'s text are correct');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('2. Should check upload page ui elements visibility and texts', async function () {
        try {
            await page.clickOnBtnUpload();
            expect(await page.checkElementsVisibility(await page.uploadElements)).toBe(true, 'upload page ui  elements are visibile');
            expect(await page.checkElementsText(await page.uploadElements)).toBe(true, 'upload page ui element\'s text are correct');
            expect(await page.checkElementsUrl(await page.uploadElements)).toBe(true, 'upload page url are correct');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('3. Should check upload by url functionality', async function () {
        try {
            await page.uploadByUrl(URL.videoToUpload);
            await page.waitForBtnUploadToGiphyVisibilty();
            await page.clickOnBtnUploadToGiphy();
            await page.waitForBtnUploadVisibilty();
            expect(await page.getLnkUpload()).toBe(URL.videoToUpload, 'upload by url is success');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });
});
