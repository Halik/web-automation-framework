import { baseUrl } from '../../common/constants';
import { Login } from '../../../core/specs/login.page.spec';
import { Users } from '../pages';
import { ChannelPage } from '../pages';
import { URL, StatusCode } from '../../common/enums';
import { HTTPRequest } from '../../../core/helpers';

const page = new ChannelPage();
const httpRequest = new HTTPRequest(URL.searchPath);
const user = Users.Alex;
const validSearch = user.testData['validSearch'];
const invalidSearch = user.testData['invalidSearch'];
Login.sharedSetup(user, baseUrl);

describe('Channel Api: Tests: ', () => {

    beforeAll(async () => {
        try {
            await page.waitForBtnUploadVisibilty();
        }
        catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('1. Should check http get request for search functionality', async function () {
        try {
            const response = await httpRequest.httpGet();
            expect(response.meta.status).toBe(StatusCode.SUCCESS, 'search get request is success');
            expect(response.pagination.total_count).not.toBe(0, 'total count is not null');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('2. Should check valid search result', async function () {
        try {
            await page.fillTxtSearch(validSearch);
            await page.clickOnBtnSearch();
            await page.waitForLblChannelCountVisibilty();
            const response = await httpRequest.httpGet();
            const channels = await page.getTextLblChannel();
            const channelsCount = channels.replace(/[^0-9]/g,'');
            expect(response.pagination.count).toBe(parseInt(channelsCount), 'search result is correct');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('3. Should check invalid search result', async function () {
        try {
            const httpRequest = new HTTPRequest(URL.emptySearchPath);
            await page.fillTxtSearch(invalidSearch);
            await page.clickOnBtnSearch();
            await page.waitForLblChannelCountVisibilty(false);
            const response = await httpRequest.httpGet();
            expect(response.pagination.count).toBe(0, 'search result is empty');
            expect(await page.isLblChannelCountVisible()).toBe(false, 'channel count is not visible');
        } catch (err) {
            expect(err).toBeFalsy();
        }
    });
});
