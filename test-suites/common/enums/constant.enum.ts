export enum Group {
    UPLOAD = 'Upload'
}

export enum StatusCode {
    SUCCESS = 200
}

export enum URL {
    videoToUpload = 'https://www.youtube.com/watch?v=UAxRz830a_o',
    searchPath = 'https://api.giphy.com/v1/channels/search?q=sport&apiKey=Gc7131jiJuvI7IdN0HZ1D7nh0ow5BU6g',
    emptySearchPath = 'https://api.giphy.com/v1/channels/search?q=txtsport&apiKey=Gc7131jiJuvI7IdN0HZ1D7nh0ow5BU6g',
}
