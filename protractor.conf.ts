import { browser, Config } from 'protractor';
import * as path from 'path';
import * as fs from 'fs'
import { Browsers } from './core/configs/browser.config';
import Util from './core/helpers/util.helper';
import { argv } from './core/configs/args'

const SELENIUM_PATH = (() => {
    return path.join(process.cwd(), 'node_modules', 'webdriver-manager', 'selenium');
})(),

ieDRIVER_PATH = (() => {
    const ie_path = path.join(SELENIUM_PATH, `IEDriverServer${Browsers.ie.versions.driver}.exe`);

    if (argv.b === Browsers.ie.shortname && !fs.existsSync(ie_path)) {
        console.log('Supported version of ieServer driver for InternetExplorer is not installed.');
        console.log(`Try\n node ./node_modules/protractor/bin/webdriver-manager update --ie --versions.standalone ${Browsers.ie.versions.driver} --versions.ie ${Browsers.ie.versions.driver}`);
        process.exit(-1);
    }
    return ie_path;
})();

export const config: Config = {
    disableChecks: true,

    // ---- Timeouts ----
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md
	/* Waiting for Angular
	 Before performing any action, Protractor waits until there are no pending asynchronous tasks in your Angular application.
	 This means that all timeouts and http requests are finished. If application continuously polls $timeout or $http,
	 Protractor will wait indefinitely and time out.
	 Default timeout: 30 seconds
	 */
    allScriptsTimeout: 30000,

	/* Waiting for Page to Load
	 When navigating to a new page using browser.get, Protractor waits for the page to be loaded and the new URL to appear before continuing.
	 Default timeout: 15 seconds
	 */
    getPageTimeout: 15000,

	/* Jasmine Spec Timeout
	 If a spec (an 'it' block) takes longer than the Jasmine timeout for any reason, it will fail.
	 Default timeout: 60 seconds
	 */
    jasmineNodeOpts: {
        defaultTimeoutInterval: (120000),
        showColors: true // Use colors in the command line report.
    },

    directConnect: (function () {
        return (argv.b === Browsers.chrome.shortname || argv.b === Browsers.firefox.shortname);
    })(),

    localSeleniumStandaloneOpts: {
        jvmArgs: ['-Dwebdriver.ie.driver=' + ieDRIVER_PATH]
    },

    capabilities: (function () {
        const chrome = () => {
            return {
                browserName: Browsers.chrome.name,
                'shardTestFiles': true,
                'maxInstances': 1,
                marionette: true,
                chromeOptions: {
                    prefs: {
                        'profile.managed_default_content_settings.notifications': 1
                    }
                }
            }
        };

        const firefox = () => {
            return {
                browserName: Browsers.firefox.name,
                acceptInsecureCerts: true
            }
        };

        const safari = () => {
            return {
                browserName: Browsers.safari.name,
                //TODO: Add ability to pass server url from command line and start selenium server remotely
                seleniumAddress: 'http://192.168.1.6:61908/wd/hub/static/resource/hub.html'
            }
        };

        const ie = () => {
            return {
                browserName: Browsers.ie.name,
                'ie.ensureCleanSession': true,
            }
        };

        switch (argv.b) {
            case Browsers.chrome.shortname: {
                return chrome();
            }
            case Browsers.firefox.shortname: {
                return firefox();
            }
            case Browsers.safari.shortname: {
                return safari();
            }
            case Browsers.ie.shortname: {
                return ie();
            }
            default: {
                console.log(`browser ${argv.b} is not supported`);
                process.exit(-1);
            }
        }
    })(),

    framework: 'jasmine2',
    beforeLaunch: () => {
        if (!argv.e) {
            process.stderr.write(`You must specify -e as environment\n`);
            process.exit(1);
        }
    },

    specs: ['./test-suites/**/specs/*.js'],
    
    suites: {
       //Page: './test-suites/page/specs/*'
    },

    onPrepare: () => {
        require('./core/modules/protractor-element.extend');
        Util.defineEnv(argv);
        Util.addReporert();
        browser.manage().window().maximize();
        if (argv.b === Browsers.ie.shortname) {
			browser.ignoreSynchronization = true;
		}
    }
};
