export * from 'protractor';
import { ElementFinder, browser, protractor, ExpectedConditions } from 'protractor';
import { TimeOut } from '../enums';
import osName = require('os-name');

declare module 'protractor/built/element' {
    export interface ElementFinder {
        isVisible(): Promise<boolean>;
        clearText(timeout?: number): Promise<void>;
        setText(text: string, timeout?: number): Promise<void>;
        safeClick(timeout?: number): Promise<void>;
        waitForVisibility(timeout?: number): Promise<void>;
        waitForInvisibility(timeout?: number): Promise<void>;
        getElementCssValue(property: string, timeout?: number): Promise<string>;
        hover(timeout?: number): Promise<void>;
        hoverAndClick(timeout?: number): Promise<void>;
        hoverToLocation(location: any, timeout?: number): Promise<void>;
        isDisabled(timeout?: number): Promise<boolean>;
        getElementSize(timeout?: number): Promise<any>;
        getElementWidth(timeout?: number): Promise<number>;
        getElementHeight(timeout?: number): Promise<number>;
        getLink(timeout?: number): Promise<string>;
        getElementText(timeout?: number): Promise<string>;
        getElementAttribute(attribute: string, timeout?: number): Promise<string>;
        clickAndWait(element?: ElementFinder): Promise<void>;
    }
}

/**
 * Checks whether the element is visible or not.
 * @example
 * page.elemets.textArea().isVisible();
 * @returns {Promise<boolean>} A promise that resolves to boolean.
 */
ElementFinder.prototype.isVisible = async function (): Promise<boolean> {
    const isPresent = await this.isPresent();
    return isPresent ? await this.isDisplayed() : false;
};

/**
 * clear the input field and send text
 * @example
 * page.elemets.textArea().sendText('desiredText');
 * @param  {string} text
 */
ElementFinder.prototype.setText = async function (text: string, timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    await this.clearText();
    await this.sendKeys(text);
};

/**
 * clear the input field
 * @example
 * page.elemets.textArea().clearText();
 */
ElementFinder.prototype.clearText = async function (timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    if (osName().includes('Mac')) {
        await this.sendKeys(protractor.Key.chord(protractor.Key.COMMAND, 'a'));
        await this.sendKeys(protractor.Key.BACK_SPACE);
    } else {
        await this.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
        await this.sendKeys(protractor.Key.DELETE);
    }
};

/**
 * Safely click the element when element is clickable.
 * @example
 * page.elemets.button().safeClick();
 */
ElementFinder.prototype.safeClick = async function (timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    const locator = await this.locator().toString();
    await browser.wait(ExpectedConditions.elementToBeClickable(this), TimeOut.DEFAULT, `element  using locator: \'${locator}\' not clickable`);
    await this.click();
};

/**
 * Hover and click the element when element is clickable.
 * @example
 * page.elemets.button().hoverAndClick();
 */
ElementFinder.prototype.hoverAndClick = async function (timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    const locator = await this.locator().toString();
    await this.hover();
    await browser.wait(ExpectedConditions.elementToBeClickable(this), TimeOut.DEFAULT, `element  using locator: \'${locator}\' not clickable`);
    await this.click();
};

/**
* Wait for element visibility.
* @example
* page.elemets.button().waitForVisibility();
*/
ElementFinder.prototype.waitForVisibility = async function (timeout: number = TimeOut.DEFAULT): Promise<void> {
    const locator = await this.locator().toString();
    await browser.wait(ExpectedConditions.visibilityOf(this), timeout, `element  using locator: \'${locator}\' not visible`);
};

/**
 * Wait for element invisibility.
 * @example
 * page.elemets.button().waitForInvisibility();
 */
ElementFinder.prototype.waitForInvisibility = async function (timeout: number = TimeOut.DEFAULT): Promise<void> {
    const locator = await this.locator().toString();
    await browser.wait(ExpectedConditions.invisibilityOf(this), timeout, `element  using locator: \'${locator}\' not invisible`);
};

/**
 * Get css value for specified element
 * @example
 * page.elemets.button().getElementCssValue('color');
 * @param  {string} property
 * @returns {Promise<string>} A promise that resolves to string.
 */
ElementFinder.prototype.getElementCssValue = async function (property: string, timeout?: number): Promise<string> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    return this.getCssValue(property);
};

/**
 * Hover specified element
 * @example
 * page.elements.button().hover()
 */
ElementFinder.prototype.hover = async function (timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    await browser.actions().mouseMove(this).perform();
};

/**
 * Hover specified element to location
 * @example
 * page.elements.button().hover({x = 10, y = 20})
 */
ElementFinder.prototype.hoverToLocation = async function (location: any = {}, timeout?: number): Promise<void> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    await browser.actions().mouseMove(this, location).perform();
};


/**
 * Checks disabled element state
 * @example
 * page.element.button().isDisabled()
 * @returns {Promise<boolean>} A promise that resolves to boolean.
 */
ElementFinder.prototype.isDisabled = async function (timeout?: number): Promise<boolean> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    return await this.isEnabled() ? false : true;
};

/**
 * Get element size
 * @example
 * page.element.button().getElementSize()
 * @returns {Promise<any>} A promise that resolves to any.
 */
ElementFinder.prototype.getElementSize = async function (timeout?: number): Promise<any> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    return await this.getSize();
};

/**
 * Get element width
 * @example
 * page.element.button().getElementWidth()
 * @returns {Promise<number>} A promise that resolves to number.
 */
ElementFinder.prototype.getElementWidth = async function (timeout?: number): Promise<number> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    const size = await this.getSize();
    return size.width;
};

/**
 * Get element height
 * @example
 * page.element.button().getElementHeight()
 * @returns {Promise<number>} A promise that resolves to number.
 */
ElementFinder.prototype.getElementHeight = async function (timeout?: number): Promise<number> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    const size = await this.getSize();
    return size.height;
};

/**
 * Get element href attribute value
 * @example
 * page.element.button().getLink()
 * @returns {Promise<string>} A promise that resolves to string.
 */
ElementFinder.prototype.getLink = async function (timeout?: number): Promise<string> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    const url = await this.getAttribute('href');
    return url;
};

/**
 * Get element text
 * @example
 * page.element.button().getText()
 * @returns {Promise<string>} A promise that resolves to string.
 */
ElementFinder.prototype.getElementText = async function (timeout?: number): Promise<string> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    return this.getText();
};

/**
 * Get element attribute
 * @example
 * page.element.button().getElementAttribute('value')
 * @returns {Promise<string>} A promise that resolves to string.
 */
ElementFinder.prototype.getElementAttribute = async function (attribute: string, timeout?: number): Promise<string> {
    await this.waitForVisibility(timeout)
        .catch((err) => Promise.reject(err));
    return this.getAttribute(attribute);
};

/**
 * Click element and wait for visivility of another element
 * @param element - element to wait for visibility
 * @param timeout - timeout
 */
ElementFinder.prototype.clickAndWait = async function (element?: ElementFinder): Promise<void> {
    await this.safeClick();
    if (element)
        await element.waitForVisibility();
};

export * from 'protractor';
