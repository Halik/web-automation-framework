import { DataBase } from '../helpers';

describe('DB Connection Tests:', () => {
    const dbCRM = new DataBase('CRM');
    dbCRM.sharedSetup();

    it('check execute query', async () => {
        try {
            const result = await dbCRM.connection.query('select * from Users where US_Login=\'ricky.falgoust\'');
            expect(result[0].US_Login).toBe('ricky.falgoust');
        }
        catch (err) {
            expect(err).toBeFalsy();
        }
    });

    it('check execure procedure', async () => {
        try {
            await dbCRM.executeProcedure('usp_QA_Set2FAAttemptsByUsLogin', ['ricky.falgoust', 1]);
            const result = await dbCRM.connection.query('select * from   D_LastLogin..AuthSubscription where UserID = \'619570\'');
            expect(result[0].AS_TwoFASuggestionAttempts).toBe(1);
        }
        catch (err) {
            expect(err).toBeFalsy();
        }
    });
});
