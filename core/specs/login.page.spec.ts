import { LoginPage } from '../pages';
import { UserInterface } from '../interfaces';

export class Login {
    public static notReady: boolean;

    public static sharedSetup(user: UserInterface, url: string, skip?: boolean): void {
        const loginPage = new LoginPage(user, url, skip);

        beforeAll(async () => {
            await loginPage.login()
                .catch((err) => this.notReady = err)
        })

        it('check login', async () => {
            expect(this.notReady).toBeFalsy();
        })

        afterAll(async () => {
            //await loginPage.logOut()
                //.catch((err) => expect(err).toBeFalsy());
        })
    }
}
