export enum TimeOut {
  DEFAULT = 120000
}

export const ErrRequired = 'Required';
