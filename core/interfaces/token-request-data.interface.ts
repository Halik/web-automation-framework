export interface TokenRequestData {
    method: string;
    clientID: string;
    clientSecret: string;
    url: string;
    grantType: string;
}
