import { ElementFinder, ElementArrayFinder } from 'protractor';
import { CssInterface } from '../interfaces';

export interface ElementInterface  {
    name: string;
    dataQt?: string;
    css?: string;
    isVisible?: boolean;
    postfix?: boolean;
    text?: string;
    isSelected?: boolean;
    placeholder?: string;
    group?: string | string[];
    index?: number;
    url?: string;
    validator?: ValidatorInterface;
    error?: ElementInterface
    cssProp?: CssInterface
}

export interface ElementFunction {
    (index?: number): ElementFinder;
    data: ElementInterface;
    all: ElementArrayFinder;
    err?: ElementFinder;
}

export interface ValidatorInterface {
    invalidPattern?: string[];
    validPattern?: string[];
    validInput?: string[];
    invalidInput?: string[];
    onlyNumeric?: boolean;
    onlyAlphabetic?: boolean;
    required?: boolean
}
