import {ElementFinder} from 'protractor';

export interface SelectInterface  {
    dropDown: ElementFinder;
    dropDownList?: ElementFinder;
    tag?: string
}
