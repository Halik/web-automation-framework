import { LoginPageElements, LoginPageElementsInterface, BasePage } from '../pages';
import { UserInterface } from '../interfaces';
import { browser } from 'protractor';

export class LoginPage extends BasePage {
    private user: UserInterface;
    private url: string;
    public elements: LoginPageElementsInterface;

    constructor(loginParam: UserInterface, url: string, skip?: boolean) {
        super(LoginPageElements);
        this.url = url;
        this.user = loginParam;
    }

    public async login(): Promise<void> {
        await browser.waitForAngularEnabled(false);
        await browser.get(this.url);
        await this.elements.txtUserName().waitForVisibility();
        await this.elements.txtUserName().setText(this.user.username);
        await this.elements.txtPassword().setText(this.user.password);
        await this.elements.btnLogin().safeClick();
    }

    public async logOut(): Promise<void> {
        await this.elements.btnLogout().safeClick();
    }
}
