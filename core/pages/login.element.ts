import { ElementInterface, ElementFunction } from '../interfaces/element.interface';

export const LoginPageElements: ElementInterface[] = [
    {
        name: 'btnLogin',
        css: 'button.sc-jffIyK.form-components__CTAButton-abosvr-3.login-form__CTAButton-sc-1vrkdbu-3.htiKJt.cZNkBb.gwMucH'
    },
    {
        name: 'btnLogout',
        css: ''
    },
    {
        name: 'txtUserName',
        css: 'div:nth-child(1) > div > input'
    },
    {
        name: 'txtPassword',
        css: 'div:nth-child(2) > div > input'
    },
];

export interface LoginPageElementsInterface {
    btnLogin: ElementFunction,
    txtUserName: ElementFunction,
    txtPassword: ElementFunction,
    btnLogout: ElementFunction,
}