import { $, $$, element, by, ElementFinder } from 'protractor';
import {ElementFunction, ElementInterface} from "../interfaces/element.interface";
import * as _ from "lodash";

export class BasePage {
    private elementsPOM;
    public elements = {};

    constructor(elementsPOM) {
        this.elementsPOM = elementsPOM;
        this.defineElements(elementsPOM);
    }

    /**
     * This private method defines elements by given data-qts and css
     * @param elementsPOM All the elements that collected in POM as Interface Array
     * @returns {void}
     */
    private defineElements(elementsPOM: ElementInterface[]): void {
        elementsPOM.forEach((item) => {
            const elementFunction: ElementFunction = (index?: number) => {
                if (item.dataQt) {
                    if (item.postfix) {
                        return element(by.dataQt(`${item.dataQt + index}`));
                    } else {
                        return element(by.dataQt(`${item.dataQt}`));
                    }
                } else if (item.css) {
                    if (item.postfix) {
                        return $(`${item.css + '(' + index + ')'}`);
                    } else {
                        return $(`${item.css}`);
                    }
                } else {
                    throw(`Error: Either dataQt or css should be provided for the element ${item.name}`)
                }
            };
            if(item.css) {
                elementFunction.all = $$(item.css)
            } else {
                elementFunction.all = $$(`[data-qt*=\"${item.dataQt}\"]`)
            }
            elementFunction.data = item;
            this.elements[item.name] = elementFunction;
            if(item.error) {
                elementFunction.err = element(by.dataQt(`${item.error.dataQt}`))
            }

        });
    }

    /**
     * This public method get element according to specified query
     * @param query object that matches with element defined in elementsPOM
     * @returns {any} found element
     */
    public getElement(query: { [key: string]: any }): ElementInterface {
        return _.find(this.elementsPOM, _.matches(query));
    }

    /**
     * This public method checks visible elements of default page
     * @returns {Promise<boolean | string[]>} Promise resolves to boolean true in case of success or resolves to errors array in case of fail
     */
    public async checkDefaultUIElementsVisibility(): Promise<boolean | string[]> {
        let err: string[] = [];
        let index: number = 0;
        const visibleElements: ElementInterface[] = this.elementsPOM.filter((element: ElementInterface) => {
            return element.isVisible;
        });

        for (const item of visibleElements) {
            try {
                let isVisible = await this.elements[item.name]().isVisible();
                if (!isVisible) {
                    err.push(`\nError: The \"${item.name}\" element is not visible`);
                }
                index++;
                if (index === visibleElements.length && err.length) {
                    return err
                } else if (index === visibleElements.length) {
                    return true
                }
            } catch (e) {
                index += 1;
                err.push(`\n${e}`);
                if (index === visibleElements.length) {
                    return err;
                }
            };
        }
    }

    /**
     * This public method checks element visibility/invisibility
     * @param elArray Element names array which exists in POM and which visibility should be checked
     * @param isVisible boolean value defining if elements should be visible/invisible
     * @returns {Promise<string[] | boolean>} Promise that resolves to true when successful or string[] when there are errors
     */
    public async checkElementsVisibility(elArray: ElementInterface[], isVisible: boolean = true): Promise<string[] | boolean> {
        let errors: string[] = [];
        let index: number = 0;

        for (const el of elArray) {
            try {
                if (isVisible !== await this.elements[el.name]().isVisible()) {
                    errors.push(`\nError: Check the element \"${el.name}\" is visible: expected ${isVisible} to be ${!isVisible}`)
                }
            } catch (err) {
                errors.push(`\n${err}`);
            }
            index++;
        }
        if (errors.length !== 0 && index === elArray.length) {
            return errors;
        } else if (index === elArray.length) {
            return true;
        }
    }

    /**
     * This public method checks every element text in given array
     *
     * @param elArray: array of elements typeof ElementInterface
     * @returns {Promise<boolean | string[]>} Promise that resolves to boolean true if check is passed and to string[] if there are errors
     */
    public async checkElementsText(elArray: ElementInterface[]): Promise<boolean | string[]> {
        let errors: string[] = [];
        let index = 0;

        const texts: ElementInterface[] = elArray.filter((element: ElementInterface) => {
            return element.text || element.text === "";
        });

        for (const el of texts) {
            try {
                let elTxt = await this.elements[el.name]().getText();
                if (elTxt.trim() !== el.text) {
                    errors.push(`\nError: Checking text of the element \"${el.name}\": expected \"${elTxt}\" to be \"${el.text}\"`)
                }
            } catch (err) {
                errors.push(`\n${err}`);
            }
            index++;
            if (errors.length !== 0 && index === texts.length) {
                return errors;
            } else if (index === texts.length) {
                return true;
            }
        }
    }

    /**
     * This public method checks every element url in given array
     *
     * @param elArray: array of elements typeof ElementInterface
     * @returns {Promise<boolean | string[]>} Promise that resolves to boolean true if check is passed and to string[] if there are errors
     */
    public async checkElementsUrl(elArray: ElementInterface[]): Promise<boolean | string[]> {
        let errors: string[] = [];
        let index = 0;

        const urls: ElementInterface[] = elArray.filter((element: ElementInterface) => {
            return element.url;
        });

        for (const el of urls) {
            try {
                let elUrl = await this.elements[el.name]().getAttribute('href');
                if (elUrl !== el.url) {
                    errors.push(`\nError: Checking url of the element \"${el.name}\": expected \"${elUrl}\" to be \"${el.url}\"`)
                }
            } catch (err) {
                errors.push(`\n${err}`);
            }
            index++;
            if (errors.length !== 0 && index === urls.length) {
                return errors;
            } else if (index === urls.length) {
                return true;
            }
        }
    }

    /**
     * This public method checks all visible elements texts of default page
     * @returns {Promise<boolean | string[]>} Promise resolves to boolean true in case of success and resolves to errors array in case of fail
     */
    public async checkDefaultTextsOnPage(): Promise<boolean | string[]> {
        let err: string[] = [];
        let index: number = 0;

        const texts: ElementInterface[] = this.elementsPOM.filter((element: ElementInterface) => {
            return element.text && element.isVisible;
        });

        for (const item of texts) {
            try {
                let text = await this.elements[item.name]().getText();
                if (text !== item.text) {
                    err.push(`\nError: Expected \"${text}\" to be \"${item.text}\"`)
                }
                index += 1;
                if (index === texts.length && err.length) {
                    return err
                } else if (index === texts.length) {
                    return true;
                }
            } catch (e) {
                index += 1;
                err.push(`\n${e}`);
                if (index === texts.length) {
                    return err;
                }
            }
        }
    };

    /**
     * Checks whether the element is enabled or not.
     *
     * @example
     * page.checkUIElementsDisabledEnabledState(elementsArray: ElementInterface[]);
     *
     * @param elArr: array of elements typeof ElementInterface
     * @param isEnabled: boolean value true if element should be enabled or false if element should be disabled
     * @returns {Promise<boolean | string[]>} A promise that resolves to boolean true when check is successful and string[] when there are errors.
     */
    public async checkUIElementsDisabledEnabledState(elArr: ElementInterface[], isEnabled: boolean = true): Promise<string[] | boolean> {
        let index: number = 0;
        let errors: string[] = [];

        for (const el of elArr) {
            try {
                if (el.index) {
                    for (const i of Array.from(Array(el.index).keys())) {
                        if (isEnabled !== await this.isElementEnabled(this.elements[el.name](i))) {
                            errors.push(`\nError: Element \"${el.name}\" is enabled: expected ${isEnabled} to be ${!isEnabled}`)
                        }
                    }
                } else {
                    if (isEnabled !== await this.isElementEnabled(this.elements[el.name]())) {
                        errors.push(`\nError: Element \"${el.name}\" is enabled: expected ${isEnabled} to be ${!isEnabled}`)
                    }
                }
            } catch (err) {
                errors.push(`\n${err}`)
            }
            index++
        }
        if (index === elArr.length && errors.length !== 0) {
            return errors;
        } else if (index === elArr.length) {
            return true;
        }
    }

    /**
     * This public method checks all by default visible input element placeholders
     * @returns {Promise<boolean | string[]>} Promise resolves to boolean true in case of success and resolves to errors array in case of fail
     */
    public async checkPlaceholder(): Promise<boolean | string[]> {
        let err: string[] = [];
        let index: number = 0;
        const elements: ElementInterface[] = this.elementsPOM.filter((element: ElementInterface) => {
            return element.placeholder && element.isVisible;
        });

        for (const elm of elements) {
            try {
                let placeholder = await this.elements[elm.name]().getAttribute('placeholder');
                if (placeholder !== elm.placeholder) {
                    err.push(`\nError: Checking placeholder for element \"${elm.name}\", expected \"${placeholder}\" to be \"${elm.placeholder}\"`)
                }
                index += 1;
                if (index === elements.length && err.length) {
                    return err
                } else if (index === elements.length) {
                    return true;
                }
            } catch (e) {
                index += 1;
                err.push(`\n${e}`);
                if (index === elements.length) {
                    return err;
                }
            }
        }
    };

    /**
     * This public method returns array of the elements that belongs to specified group
     * @param group: group name
     * @returns {ElementInterface[]} array of elements
     */
    public async getElementsArrayByGroup(group: string): Promise<ElementInterface[]> {
        let elements: ElementInterface[] = [];
        const elms: ElementInterface[] = this.elementsPOM.filter((element: ElementInterface) => {
            if(element.group  && typeof(element.group) === 'string') {
                return element.group === group
            } else if(element.group) {
                return element.group.includes(group);
            }
        });
        for (const element of elms) {
            elements.push(element);
        }
        return elements;
    }

    /**
     * This public method returns the element which formed as a result of element names specified in parameters.
     * @param args names of elements need to be concatenated.
     * @returns {ElementFinder} formed element
     */
    public getByParent(...args: ElementFunction[]): ElementFinder {
        let locator: string = '';
        args.forEach(name => {
            locator = locator + this.getSelector(name);
        });
        return element(by.css(locator));
    }

    /**
     * This public method returns the element which formed as a result of two elements specified as parameters
     * @param parent  parent element
     * @param child child element
     * @returns {ElementFinder} formed element
     */
    public getByChild(parent: ElementFinder, child: ElementFunction): ElementFinder {
        let locator = this.getSelector(child);
        return parent.element(by.css(locator));
    }

    /**
     * This private method returns css selector of the element
     * @param element: element
     * @returns {string} css selector
     */
    private getSelector(element: ElementFunction): string {
        let elementSelector: string;

        if(element.data.dataQt){
            elementSelector = `[data-qt=\"${element.data.dataQt}\"] `
        } else if(element.data.css){
            elementSelector = element.data.css;
        }
        return elementSelector
    }

    /**
     * This private method returns true id element is enabled and false if element is disabled
     * @param element: element
     * @returns {boolean} state of the element
     */
    private async isElementEnabled(elm: ElementFinder): Promise<boolean> {
        let isEnb = await elm.isEnabled();
        let atr = (await elm.getAttribute('class')).includes('--disabled');
        if (isEnb && !atr) {
            return true;
        } else {
            return false;
        }
    }
}
