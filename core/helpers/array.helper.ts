/**
 * This class provides Arrays helper functions
 */
export class Array {

    /*
     * This public method gives the elements that both arrays share in common
     * @returns Array with Common elements
     */
    public static intersection = (arr1: string[], arr2: string[]) => arr1.filter(x => arr2.includes(x));

    /**
    * This public method checks if there are matched elements in the both arrays
    * @param arr1 array of strings
    * @param arr2 array of strings
    * @returns { boolean }: returns true if all the elements are matching , and false if at least one is not matching
    */
    public static isElementsMatched(arr1: string[], arr2: string[]): boolean {
        return arr2.reduce((initial, current) => {
            const splitedElements = current.toLowerCase().split(' ');
            const count = arr1.reduce((init, curr) => {
                if (splitedElements.includes(curr)) ++init;
                return init;
            }, 0)
            initial = (count === arr1.length) && initial;
            return initial;
        }, true as boolean);
    }

    /**
    * This public method checks if each valu of arry contais specified string
    * @param searchStr:    string
    * @param optionsList:  array of strings
    * @returns { boolean }: returns true if all the elements are containing specified string, and false if at least one is not containing the string
    */
    public static eachContains(optionsList: string[], searchStr: string): boolean {
        for (const val of optionsList) {
            if (!val.toLowerCase().includes(searchStr)) { return false };
        }
        return true;
    }

    /**
    * This public method checks if given array contains duplicate values 
    * @param arr array
    * @returns { boolean }: returns true if there are duplicated elements, and false if there are no duplicated elements
    */
    public static hasDuplicatedElements(arr: any[]): boolean {
        const newArray = new Set(arr);
        if (newArray.size === arr.length) {
            return false;
        } else {
            return true;
        }
    }
}
