import * as path from 'path';
import * as fs from 'fs';

export default class Util {
    static addReporert() {
        // Require protractor-beautiful-reporter to generate reports.
        const HtmlReporter = require('protractor-beautiful-reporter')
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: 'test-results',
            preserveDirectory: true, // Preserve base directory
            screenshotsSubfolder: 'screenshots',
            jsonsSubfolder: 'jsons', // JSONs Subfolder
            takeScreenShotsForSkippedSpecs: true, // Screenshots for skipped test cases
            takeScreenShotsOnlyForFailedSpecs: false, // Screenshots only for failed test cases
            docTitle: 'Test Automation Execution Report', // Add title for the html report
            docName: 'TestResult.html', // Change html report file name
            getBrowserLogs: false, // Store Browser logs
            gatherBrowserLogs: false
        }).getJasmine2Reporter());
    }

    static defineEnv(argv: any) {
        const configPath = path.join(process.cwd(), 'core/configs');
        const coreGlobal = <any>global;
        const env = argv.e;
        if (fs.existsSync(path.join(configPath, env + '.config.json'))) {
            coreGlobal.config = require(path.join(configPath, env + '.config.json'));
        }
        else {
            process.stderr.write(`couldn't find config for '${env}' environment`);
            process.exit(1);
        }
    }
}
