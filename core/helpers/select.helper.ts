import { By, ElementFinder } from 'protractor';
import {SelectInterface} from '../interfaces';

export class Select {
    private tag: string;
    private dropDownList: ElementFinder;
    private dropDown: ElementFinder;

    /*
     * Constructor initializes all the private parameters of the class
     */
    constructor(params: SelectInterface){
        this.dropDown = params.dropDown;
        if(params.dropDownList) {
            this.dropDownList = params.dropDownList;
        } else {
            this.dropDownList = params.dropDown
        }
        if(params.tag) {
            this.tag = params.tag;
        } else {
            this.tag = 'li'
        }
    }

    /*
     * This public method returns list of options from specified dropDown
     * @returns {Promise<string[]>}: All the options
     */
    public async getOptions(): Promise<string[]> {
        const options: string[] = [];
        var index: number = 0;

        await this.dropDown.click();
        const opts = await this._getOptions();
        for(const el of opts) {
            const txt = await el.getText();
            options.push(txt);
            index++;
        }
        if(index === opts.length) {
            await this.dropDown.click();
            return options
        }
    }

    /*
     * This public method returns list of options from specified custom dropDown
     * @returns {Promise<string[]>}: All the options
     */
    public async getOptionsCustomDropDown(): Promise<string[]> {
        const options: string[] = [];
        var index: number = 0;

        const opts = await this._getOptions();
        for(const el of opts) {
            const txt = await el.getText();
            options.push(txt);
            index++;
        }
        if(index === opts.length) {
            return options
        }
    }

    /*
     * This public method returns selected option
     * @returns {Promise<string>}: Current option text
     */
    public async getCurrentOption(): Promise<string> {
        var index = 0;
        if(this.tag === 'option') {
            const opts = await this._getOptions();
            for(const el of opts) {
                const selected = await el.isSelected();
                if(selected) {
                    return await el.getText()
                }
                index++
            }
            if(index === opts.length){
                return Promise.reject('No selected option');
            }
        } else {
            return await this.dropDown.getText();
        }
    }

    /*
     * This public method selects option by text
     * @returns {Promise<string>}: Returns void if selection is successful and Err otherwise
     */
    public async selectOptionByText(text: string): Promise<string> {
        try {
            var index: number = 0;

            await this.dropDown.click();
            const opts = await this._getOptions();
            for (const el of opts) {
                const txt = await el.getText();
                if (txt.includes(text)) {
                    await el.click();
                    break;
                }
                index++
            }
            if (index === opts.length) {
                return Promise.reject('Err: No such option')
            }
        } catch(err) {
            return Promise.reject(err)
        }
    }

    /*
     * This public method returns first match of option with specified text
     * @param text option name
     */
    public async getOptionObject(text: string): Promise<ElementFinder> {
        var index : number = 0;
        const options = await this._getOptions();
        for (const option of options) {
            const elementText = await option.getText();
            if (elementText === text) {
                return option;
            }
            index++;
        }
        if (index === options.length) {
            return Promise.reject(`Err: No such option with text ${text}`);
        }
    }

    /*
    * This private method returns all the components from list
    * @returns {Promise<ElementFinder[]>}
    */
    private  async _getOptions(): Promise<ElementFinder[]> {
        const options = await this.dropDownList.all(By.css(this.tag));
        return options;
    }
}
