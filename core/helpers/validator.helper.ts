import { ElementInterface }  from '../interfaces';
import * as RandExp from 'randexp';
import { ErrRequired } from '../enums';
import { protractor } from 'protractor';

/**
 * This class provides helpers to validate whole form or single field by given parameters
 */
export interface returnObjectInterface {
    errors: string[],
    isTestPassed: boolean,
}

export class ValidatorHelper {
    private pageElements: any;

    /*
     * Constructor initializes all the private parameters of the class
     */
    constructor(pageElements: any) {
        this.pageElements = pageElements
    }

    /*
     * This public method validates field by given parameters
     * @param {ElementInterface} element - element needs to be validated
     * @returns {Promise<returnObjectInterface>}: status and errors if any
     */
    public async validateField(element: ElementInterface): Promise<returnObjectInterface> {
        const returnObject: returnObjectInterface = {
            errors: [],
            isTestPassed: true
        };
        if (element.validator.invalidPattern) {
            await this.validateByInvalidPatternList(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if (element.validator.validPattern) {
            await this.validateByValidPatternList(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if(element.validator.required) {
            await this.validateRequired(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if(element.validator.invalidInput){
            await this.validateByInvalidInputList(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if(element.validator.validInput) {
            await this.validateByValidInputList(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if(element.validator.onlyNumeric) {
            await this.validateOnlyNumeric(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        if(element.validator.onlyAlphabetic){
            await this.validateOnlyAlphabetic(element)
                .catch((err) => {
                    returnObject.errors.push(err);
                    returnObject.isTestPassed = false;
                })
        }
        return returnObject;
    }

    /*
     * This public method validates form fields by given parameters
     * @param {ElementInterface[]} elements - list of form elements needs to be validated
     * @returns {Promise<returnObjectInterface>}: status and errors if any
     */
    public async validateForm(elements?: ElementInterface[]): Promise<returnObjectInterface> {
        const returnObject:returnObjectInterface = {
            errors: [],
            isTestPassed: true
        };
        const els: ElementInterface[] = elements.filter((element: ElementInterface) => {
            return element.validator;
        });

        for (const item of els) {
            try{
                const result: returnObjectInterface = await this.validateField(item);
                returnObject.errors = returnObject.errors.concat(result.errors);
                returnObject.isTestPassed = returnObject.isTestPassed && result.isTestPassed

            } catch(err) {
                returnObject.errors.push(err);
                returnObject.isTestPassed = false;
            }
        }
        return returnObject;
    }

    /*
     * This private method validates field by given regexp
     * @param {ElementInterface} element - element needs to be validated
     * @param {string} pattern - regexp of valid input
     * @returns {Promise<string>}: error message is eny
     */
    private async validateByValidPattern(element: ElementInterface, pattern: string): Promise<string> {
        try {
            const input = this.generateStringByPattern(pattern);
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            if(await this.pageElements[element.name].err.isVisible()){
                return Promise.reject(`Error: The error is unexpectedly displayed for the field \'${element.name}\' and value \'${input}\'\n`)
            }
        } catch (err) {
            return Promise.reject(err)
        }
    };

    /*
     * This private method validates field by given regexp
     * @param {ElementInterface} element - element needs to be validated
     * @param {string} pattern - regexp of invalid input
     * @returns {Promise<string>}: status and errors if any
     */
    private async validateByInvalidPattern (element: ElementInterface, pattern: string): Promise<string> {
        try {
            const input = this.generateStringByPattern(pattern);
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            if(!await this.pageElements[element.name].err.isVisible()){
                return Promise.reject(`Error: The error is unexpectedly missing for the field \'${element.error.name}\' and value \'${input}\'\n`);
            }else{
                const text = await this.pageElements[element.name].err.getText();
                if(text !== element.error.text) {
                    return Promise.reject(`Error: Incorrect error message is displayed for the field \'${element.name}\' and value \'${input}\'\nActual result: \'${text}\', Expected result: \'${element.error.text}\'\n`);
                }
            }
        } catch (err) {
            return Promise.reject(err)
        }
    };

    /*
    * This private method validates field by given invalid input regexp's list
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string[]>}: errors if any
    */
    private async validateByInvalidPatternList(element: ElementInterface): Promise<string[]> {
        const errors: string[] = [];

        for (const i of element.validator.invalidPattern) {
            try {
                await this.validateByInvalidPattern(element, i)
            } catch (err){
                errors.push(err)
            }
        }
        if(errors.length === 0) {
            return errors
        } else {
            return Promise.reject(errors)
        }
    }

    /*
    * This private method validates field by given valid input regexp's list
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string[]>}: errors if any
    */
    private async validateByValidPatternList(element: ElementInterface): Promise<string[]> {
        const errors: string[] = [];

        for (const i of element.validator.validPattern) {
            try {
                await this.validateByValidPattern(element, i)
            } catch (err){
                errors.push(err)
            }
        }
        if(errors.length === 0) {
            return errors
        } else {
            return Promise.reject(errors)
        }
    }

    /*
     * This private method validates field by given input
     * @param {ElementInterface} element - element needs to be validated
     * @param {string} input - valid input
     * @returns {Promise<string>}: error message is eny
     */
    private async validateByValidInput(element: ElementInterface, input: string): Promise<string> {
        try {
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            if(await this.pageElements[element.name].err.isVisible()){
                return Promise.reject(`Error: The error is unexpectedly displayed for the field \'${element.name}\' and value \'${input}\'\n`)
            }
        } catch (err) {
            return Promise.reject(err)
        }
    };

    /*
     * This private method validates field by given input
     * @param {ElementInterface} element - element needs to be validated
     * @param {string} input - invalid input
     * @returns {Promise<string>}: error message is eny
     */
    private async validateByInvalidInput (element: ElementInterface, input: string): Promise<string> {
        try {
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            if(!await this.pageElements[element.name].err.isVisible()){
                return Promise.reject(`Error: The error is unexpectedly missing for the field \'${element.error.name}\' and value \'${input}\'\n`);
            }else{
                const text = await this.pageElements[element.name].err.getText();
                if(text !== element.error.text) {
                    return Promise.reject(`Error: Incorrect error message is displayed for the field \'${element.name}\' and value \'${input}\'\nActual result: \'${text}\', Expected result: \'${element.error.text}\'\n`);
                }
            }
        } catch (err) {
            return Promise.reject(err)
        }
    };

    /*
    * This private method validates field by given invalid inputs list
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string[]>}: errors if any
    */
    private async validateByInvalidInputList(element: ElementInterface): Promise<string[]> {
        const errors: string[] = [];

        for (const i of element.validator.invalidInput) {
            try {
                await this.validateByInvalidInput(element, i)
            } catch (err){
                errors.push(err)
            }
        }
        if(errors.length === 0) {
            return errors
        } else {
            return Promise.reject(errors)
        }
    }

    /*
    * This private method validates field by given valid inputs list
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string[]>}: errors if any
    */
    private async validateByValidInputList(element: ElementInterface): Promise<string[]> {
        const errors: string[] = [];

        for (const i of element.validator.validInput) {
            try {
                await this.validateByValidInput(element, i)
            } catch (err){
                errors.push(err)
            }
        }
        if(errors.length === 0) {
            return errors
        } else {
            return Promise.reject(errors)
        }
    }

    /*
    * This private method validates if field is required
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string>}: error message
    */
    private async validateRequired(element: ElementInterface): Promise<string> {
        try{
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
            await this.pageElements[element.name]().sendKeys(protractor.Key.DELETE);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            if(!await this.pageElements[element.name].err.isVisible()){
                return Promise.reject(`Error: The required error is unexpectedly missing for the field \'${element.name}\'\n`);
            }else{
                const text = await this.pageElements[element.name].err.getText();
                if(text !== ErrRequired) {
                    return Promise.reject(`Error: Incorrect error message is displayed for the field \'${element.name}\'\nActual result: \'${text}\', Expected result: \'${ErrRequired}\'\n`);
                }
            }
        } catch(err) {
            return Promise.reject(err)
        }
    };

    /*
    * This private method validates that the field can be filed only with numbers
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string>}: error message
    */
    private async validateOnlyNumeric(element: ElementInterface): Promise<string> {
        try {
            const pattern = '([A-Z]|[a-z])';
            const input = this.generateStringByPattern(pattern);
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            const text = await this.pageElements[element.name]().getText();
            const value = await this.pageElements[element.name]().getAttribute('value');
            if (text !== '' || value != '') {
                return Promise.reject(`Error: The numeric field \'${element.name}\' contains text \'${text}${value}\'\n`);
            }
        } catch(err) {
            return Promise.reject(err)
        }
    }

    /*
    * This private method validates that the field can be filed only with letters
    * @param {ElementInterface} element - element needs to be validated
    * @returns {Promise<string>}: error message
    */
    private async validateOnlyAlphabetic(element: ElementInterface): Promise<string> {
        try {
            const pattern = '([0-9])';
            const input = this.generateStringByPattern(pattern);
            await this.pageElements[element.name]().waitForVisibility();
            await this.pageElements[element.name]().setText(input);
            await this.pageElements[element.name]().sendKeys(protractor.Key.TAB);
            const text = await this.pageElements[element.name]().getText();
            const value = await this.pageElements[element.name]().getAttribute('value');
            if (text !== '' || value != '') {
                return Promise.reject(`Error: The alphabetic field \'${element.name}\' contains text \'${text}${value}\'\n`);
            }
        } catch(err) {
            return Promise.reject(err)
        }
    }

    /*
    * This private method generates random string by given regexp
    * @param {string} pattern - regular expression
    * @returns {string}: generated string
    */
    private generateStringByPattern = (pattern: string): string => {
        const randexp = new RandExp(pattern);
        return randexp.gen();
    };

}
