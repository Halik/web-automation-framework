/**
 * Created by Aleksandr Hakobyan 7/26/2019
 */

import { browser, ExpectedConditions } from 'protractor';
import { TimeOut } from '../enums/constant.enum';

/**
 * This class provides browser helper functions
 */
export class Browser {

    /**
     * This method switch to the tab with the given index
     * @param index
     */
    public static async switchTab(index: number): Promise<void> {
        const handles = await browser.getAllWindowHandles();
        await browser.switchTo().window(handles[index]);
    }

    /**
    * This method scrolls page Up/Down to the given coordinates
    */
    public static async scroll(x: number, y: number): Promise<void> {
        await browser.executeScript(`window.scrollTo(${x},${y})`);
    }

    /**
     * This method waits for url to contain given suburl param
     */
    public static async waitForUrlContains(url: string): Promise<void> {
        await browser.wait(ExpectedConditions.urlContains(url), TimeOut.DEFAULT, `wait for \'${url}\' page`);
    }

    /**
     * This method performs refresh/back/forward actions for browser
     * @param action browser action (can have the following refresh/back/forward)
     */
    public static async action(action: string): Promise<void> {
        await browser.navigate()[action]()
    }

     /**
     * This method gets current browser name
     */
    public static async getBrowser(): Promise<string> {
        const caps =  await browser.getCapabilities();
        return await caps.get('browserName');
    }

    /**
     * This method gets new opened tab url
     */
    public static async getTabUrlByIndex(index = 1): Promise<string> {
        await this.switchTab(index);
        const url = await browser.getCurrentUrl();
        await browser.close();
        await this.switchTab(0);
        return url;
    }
}
