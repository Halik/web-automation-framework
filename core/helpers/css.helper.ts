import { ElementInterface, ResultInterface } from '../interfaces';
import { Css } from '../enums';
import { Browser } from '../helpers';

/**
 * This class provides helpers to validate web element css properties
 */
export class CssHelper {

    /*
    * Constructor initializes all the private parameters of the class
    */
    constructor(pageElements: any) {
        this.pageElements = pageElements;
    }

    /*
     * This public method returns element css property
     * @param {ElementInterface} element - web element
     * @param {string} property -  property that needs to be returned
     * @returns {Promise<string>}: status and errors if any
     */
    public async getElementProperty(element: ElementInterface, property: string) : Promise<string> {
        const p = await this.pageElements[element.name]().getElementCssValue(property);
        return await this.adjustBrowser(p);
    }

    /*
     * This public method validates element css properties
     * @param {ElementInterface} element - web element
     * @returns {Promise<ResultInterface>}: resolves to ResultInterface object
     */
    public async validateElementProperties(element: ElementInterface): Promise<ResultInterface> {
        const result: ResultInterface = {
            error: [],
            status: true,
        };
        const toCheck = this.getCssPropertyArray(element);
        for (const p of toCheck) {
            try {
                let val = await this.getElementProperty(element, Css[p]);
                if(p === 'fontFamily') {
                    val = this.getFontFamily(val)
                }
                if (val !== element.cssProp[p]) {
                    result.status = false;
                    result.error.push(`Error: Check element \'${element.name}\' property \'${p}\': expected ${val} to be ${element.cssProp[p]}\n`)
                }
            } catch (err) {
                result.status = false;
                result.error.push(err)
            }
        }
        return result
    }

    /*
     * This public method validates element css properties
     * @param {ElementInterface[]} elements - web element array
     * @returns {Promise<ResultInterface>}: resolves to ResultInterface object
     */
    public async validateElementListProperties(elements: ElementInterface[]): Promise<ResultInterface> {
        const result: ResultInterface = {
            error: [],
            status: true,
        };
        for(const el of elements) {
            const res = await this.validateElementProperties(el);
            result.status = result.status && res.status;
            result.error = result.error.concat(res.error)
        }
        return result
    };

    /*
     * This public method converts rgba to rgb format
     * @param {string} color - color in rgba format
     * @returns {string}: color in rgb format
     */
    public rgbaToRgbConverter(color: string) : string {
        if (color === 'transparent' || color === 'rgba(0, 0, 0, 0)')  return 'rgb(0, 0, 0, 0)';
        return 'rgb(' + color.substring(5, color.length-4) + ')';
    }

    /*
     * This public method returns property adjusted with browser
     * @param {string} property - property
     * @returns {string}: property adjusted with browser
     */
    public async adjustBrowser(property: string): Promise<string> {
        if(property.includes('rgba') || property.includes('transparent')) {
            return this.rgbaToRgbConverter(property);
            const browserName = await Browser.getBrowser();
            if (browserName !== 'firefox') return this.rgbaToRgbConverter(property);
            else return property;
        }
        else return property;
    };

    private pageElements: any;

    /*
     * This private method converts hex to RGB
     * @param {string} hex
     * @returns {string}: rgb value
     */
    private static hexToRgbConverter(hex) : string {
        hex = hex.replace('#','');
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
        const result = 'rgb('+r+','+g+','+b+')';
        return result;
    }

    /*
     * This private method returns array of css properties of elements specified in POM
     * @param {ElementInterface} element -  element
     * @returns {string[]}:  array of properties
     */
    private getCssPropertyArray(element: ElementInterface) {
        if(element.cssProp) {
            return Object.keys(element.cssProp)
        } else {
            throw `Error: The element \'${element.name}\' has no property cssProp`
        }
    }

    /*
     * This private method converts font family property to same format for all browsers
     * @param {string} property -  value
     * @returns {string}:  converted value
     */
    private getFontFamily(property: string): string {
        return property.replace(/\'/g, '').toLowerCase().replace(/,.*/, '');
    }
}
