import { ResponsePromise } from 'protractor-http-client/dist/promisewrappers';
import { TokenRequestData } from '../interfaces';
export const axios = require('axios');

/**
 * This class provides HTTP requests to fetch or save data
 */
export class HTTPRequest {
    private url: string;
    private tokenUrl: string;
    private method: string;

    private auth = {
        username: '',
        password: ''
    };

    private data = {
        grant_type: '',
    };

    /**
     * Constructor initializes the data for request 
     * @param request data
     */
    constructor(url: string, params?: TokenRequestData) {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
        this.url = url;
        if (params) {
            this.method = params.method;
            this.tokenUrl = params.url;
            this.auth.username = params.clientID;
            this.auth.password = params.clientSecret;
            this.data.grant_type = params.grantType;
        }
    }

    /**
      * This public method does post request to get access token
      * @returns {ResponsePromise}: returns response body of the post request
      */
    private async getTokenRequest(): Promise<ResponsePromise> {
        // qs allows to create nested objects within query strings
        const qs = require('qs');
        const options = {
            method: this.method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify(this.data),
            auth: this.auth,
            url: this.tokenUrl
        };

        try {
            const response = await axios(options);
            return response.data;
        } catch (error) {
            return error;
        }
    };


    /**
      * This public method does get request to get reference data
      * @param auth call getTokenRequest function
      * @returns {ResponsePromise}: returns response body of the get request
      */
    public async httpGet(): Promise<ResponsePromise> {
        try {
            if (this.tokenUrl !== undefined) {
                const responseToken = await this.getTokenRequest();
                const config = {
                    headers: {
                        Authorization: responseToken.token_type + ' ' + responseToken.access_token,
                    }
                }
                var response = await axios.get(this.url, config);
            } else {
                var response = await axios.get(this.url);

            }
            return response.data;
        } catch (error) {
            return error;
        }
    }

    /**
     * This public method does post request to get reference data
     * @param formData data of the post request
     * @returns {ResponsePromise}: returns response body of the post request
     */
    public async httpPost(formData: Object): Promise<ResponsePromise> {
        try {
            if (this.tokenUrl !== undefined) {
                const responseToken = await this.getTokenRequest();
                const config = {
                    headers: {
                        Authorization: responseToken.token_type + ' ' + responseToken.access_token,
                    }
                }
                var response = await axios.post(this.url, formData, config);
            } else {
                var response = await axios.post(this.url, formData);

            }
            return response.data;
        } catch (error) {
            return error;
        }
    }
}
