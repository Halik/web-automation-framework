/**
 *  Created by Alexander Hakobyan on 15/07/2019
 */
import { createConnection, Connection } from 'typeorm';

/**
 * This class is used for DataBase connections.
 * It provides DB helper methods.
 */
export class DataBase {
    public dbName: string;
    public connection: Connection;
    public notReady: boolean;

    /**
     * Constructor inits DB name of db connection
     * @param dbName 
     */
    constructor(dbName: string) {
        this.dbName = dbName;
    }

    /**
     * This public method creates connection with the given db name
     * @returns {Promise<void>}
     */
    public async connect(): Promise<void> {
        this.connection = await createConnection(this.dbName)
    }

    /**
    * This public method closes connection
    * @returns {Promise<void>}
    */
    public async close(): Promise<void> {
        await this.connection.close();
    }

    /**
     * This public method executes db procedure with given Procedure name and its' parameters 
     * @param name Procedure name which exists in Database 
     * @param paramArr Array of parameters for the execution of procedure
     * @returns {Promise<any>}
     */
    public async executeProcedure(name: string, paramArr: any[]): Promise<any> {
        const command: string = 'EXEC ' + name + ' ';
        for (let i = 0; i < paramArr.length; i++) {
            if (typeof paramArr[i] === 'string') {
                paramArr[i] = '\'' + paramArr[i] + '\'';
            }
        }
        return await this.connection.query(command + paramArr);
    }

    /**
     * This public method setups db connection with its Connect method in BeforeAll and Close mehod in AfterAll
     * Also check DB connection
     * @returns {void}
     */
    public sharedSetup(): void {

        beforeAll(async () => {
            await this.connect()
                .catch((err) => this.notReady = err)
        })

        it('check db connect', () => {
            expect(this.notReady).toBeFalsy();
        })

        afterAll(async () => {
            await this.close()
                .catch((err) => expect(err).toBeFalsy());
        })
    }
}
