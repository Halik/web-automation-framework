import { Environment, Browsers } from '../configs';
export const argv = require('yargs')
    .options({
        'e': {
            alias: 'env',
            describe: 'Environment',
            requiresArg: false,
            choices: Object.keys(Environment).map((key) => Environment[key].name)
        },
        'b': {
            alias: 'browser',
            describe: 'Browser',
            default: Browsers.chrome.shortname,
            choices: Object.keys(Browsers).map((key) => Browsers[key].shortname)
        }
    })
    .group(['env'], 'Environment')
    .help('h').argv;
