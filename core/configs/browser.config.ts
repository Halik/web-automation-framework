export const Browsers: any = {
  chrome: {
    name: 'chrome',
    shortname: 'gc'
  },
  safari: {
    name: 'safari',
    shortname: 'safari'
  },
  firefox: {
    name: 'firefox',
    shortname: 'ff',
  },
  ie: {
    name: 'internet explorer',
    shortname: 'ie',
    versions: {
      supported: '11',
      driver: '3.141.59'
    }
  }
};
