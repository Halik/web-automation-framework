# Protractor, Jasmine and Typescript Setup Guide
This is Test Automation framework designed using Protractor, Jasmine and TypeScript

## Framework Structure
```
├───core  # This folder contains enums, interfaces, helpers, environments,constants 
└───test-suites # This folder contains spec/test files (includes page class, elements interface, user data)
├───temp  # This folder contains JS file which are generated after compiling TypeScript files
├───test-results  # This folder contains test result (includes html report and screenshots)
```

## To Get Started

### Pre-requisites
* Download and install Chrome or Firefox browser.
* Download and install Node.js:
* Optional - Download and install any Text Editor like Visual Code/WebStorm

### Setup Scripts 
* Clone the repository into a folder
* Install Protractor: `npm install -g protractor`
* Go to Project root directory and install Dependency: `npm install`
  All the dependencies from package.json would be installed in node_modules folder.
* Update necessary binaries of webdriver-manager: `node ./node_modules/protractor/bin/webdriver-manager update` 

### Setup Env Script
* Run script  `./setup.env.sh`

### How to write Test
* Create <test-suites> folder into a folder 
* Create folder under test-suites folder as <test-suite-name>
* Create pages and specs folder under  <test-suite-name> folder 
* Add new elements, user-data and page files under <pages> folder
* Name the file as <pagename.elements>.ts, <pagename.page>.ts, <pagename.user-data>.ts 
* Add new spec files under <specs> folder
* Name the file as <testname>.spec.ts

### How to Run Test
* Run complete All Test Suites: `npm test`
* Run complete Test Suite: `npm run suite <test-suite-name>`
* Run complete Test Spec: `npm run spec temp/test-suites/<test-suite-name>/specs/<test-spec-name>`

### How to Observe Test Report
* Go to Project root directory and open test-results/TestResult.html

### How to Update local npm packages
* Go to Project root directory and run command: `npm update`